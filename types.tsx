export type RootStackParamList = {
  Root: undefined;
  NotFound: undefined;
  Signin: undefined;
  Signup: undefined;
  Home:undefined;
};

export type BottomTabParamList = {
  TabOne: undefined;
  TabTwo: undefined;
};

export type TabOneParamList = {
  TabOneScreen: undefined;
};

export type TabTwoParamList = {
  TabTwoScreen: undefined;
};


