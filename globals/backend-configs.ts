
export const API_SERVER = 'http://192.168.0.112:3000/';

export  enum ApiEndpoints{
  baseEndpoint = '/',  
  SIGNIN = 'auth/signin',
  SIGNUP = 'auth/signup',
  EDIT_USER = `user/edit/`,
  DEACTIVATE_USER='user/deactivate/',
  REACTIVATE_USER ='user/reactivate/',
  USER_AVATAR= 'user/avatar/',
  CARD = 'card',
  EDIT_CARD = 'card/edit/',
  BLOCK_CARD ='card/block/',
  UNBLOCK_CARD='card/unblock/'

}


export enum RequestMethods{
  POST= 'POST',
  GET ='GET',
  PATCH ='PATCH'
}