import { User } from "./user.entity";

  export class Card {
    
    id:string;

    holder: string;
    
    expireDate: Date;
  
    secureCode: string;
  
    isBlocked: boolean;
  
    brand: string;
  
    constructor(id:string, holder:string,expireDate:Date,secureCode:string,isBlocked:boolean,
             brand:string){
                 this.brand = brand;
                 this.expireDate = expireDate;
                 this.secureCode = secureCode;
                 this.holder = holder;
                 this.id = id;
                 this.isBlocked = isBlocked;

    }
  
  }
  