import { Address } from "./address.entity";
import { Card } from "./card.entity";

export class User{

  id: string;

  name: string;

  email: string;

  isActive: boolean;

  cards: Card[] = [];

  address?: Address;

  role:string

  constructor(id:string, name:string, email:string,isActive:boolean,role:string){
      this.name = name;
      this.id = id;
      this.email = email;
      this.isActive = isActive;
      this.role = role;
  }
 
  fromJson(json:any){
  
      this.email = json["email"];
      this.id =json["id"];
      this.isActive =json["isActive"];
      this.name =json["name"];
      this.role = json["role"];
      console.log(json["email"]);
      
      if(json["card"] != null){
        const cards:any[] = json["card"];
        cards.forEach((cardJson)=>{
          const newCard = new Card(
            cardJson["brand"],
            cardJson["expireDate"],
            cardJson["secureCode"],
            cardJson["holder"],
            cardJson["id"],
            cardJson["isBlocked"]
          );
          this.cards.push(newCard);
        });


    }
  }



}
