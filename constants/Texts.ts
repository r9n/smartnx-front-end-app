
export const appName = 'Smart NX';
export const register = 'Cadastrar';
export const login = 'Logar';
export const emailText = 'E-Mail';
export const passwordText = 'Senha';
export const name = 'Nome';
export const back = 'Back';

export const info  ='Informações';
export const menu = 'Menu';
export const invalidEmail = 'E-mail inválido';

export const invalidPassword = 'Password inválida !';

export const insertPassword = 'Informe sua senha';

export const insertEmail = 'Informe seu e-mail';

export const invalidName = 'Nome inválido!';

export const insertYourName = 'Informe seu nome;'

export const registerSucessfull = 'Cadastro realizado com sucesso';

export const loginSuccessfull  = 'Login feito com sucesso';

export const userALreadyRegistered = 'Usuário já cadastrado!';

export const unknowError = 'Ops! Um erro desconhecido ocorreu, por favor tente novamente.';

export const invalidEmailOrPassword = 'E-mail ou senah inválidos!';

export const welcome = `Bem vindo(a)`;

export const userNotFound = 'Usuário não cadastrado!';

export const changeAvatar = 'Alterar foto';