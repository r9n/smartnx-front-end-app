import * as React from 'react';
import { ImageBackground, StyleSheet } from 'react-native';
import { Text, View } from '../components/Themed';
import { Button, Divider } from 'react-native-elements';
import { appName, login, register } from '../constants/Texts';
import { loginView, signup } from '../navigation/routes';



export default function Root({navigation}) {

  return (
    <View style={styles.container}>
         
    <ImageBackground source={require('../assets/images/signup_background.png')}
                     resizeMode='cover' 
    style={styles.container}>
     <View style={styles.separator1} />
     <View style={styles.separator1} />
     <View style={styles.separator1} />
     <Text style = {styles.title}>Bem vindo ao app {appName}!</Text>
     <Text style = {styles.separator2}/>
     <Text style = {styles.subTitle}>Faça login ou cadastre-se </Text>
     <View style={styles.separator1} />
     <View style={styles.separator1} />
     <View style={styles.separator1} />
     <View style={styles.separator1} />
     <View style={styles.separator1} />
     <View style={styles.separator1} />
     <View style={styles.separator1} />
     <View style={styles.separator1} />
     <View style={styles.separator1} />
     <View style={styles.buttonGroup}>
    <View style={styles.button}>
      <Button title={register}
        onPress={() => navigation.navigate(signup)}
      />

      <Button title={login}
              onPress={() => navigation.navigate(loginView)}/>
    </View>
   
    </View>
    </ImageBackground>
    </View>
  );
}

const styles = StyleSheet.create({
  title: {    
    fontSize: 23,
    fontWeight: 'bold',
    alignContent:'center',
    textAlign:'center'
    
  },
  subTitle:{
  fontSize:18,
  fontWeight:'normal',
  textAlign:'center'
  },
  separator1: {
    marginVertical: 25,
    width: '100%',
  },
  separator2: {
    marginVertical: 10,
    height: 1,
    width: '100%',
  },
  container: {
    flex: 1,
    alignItems: 'center',
  },

  buttonGroup:{
    flex:1,
    margin:0,
   flexDirection:'row'

},
button:{
  flex:1,
  padding:0,
  marginBottom:10,
  alignContent:'space-between'
},

  
});
