import * as React from 'react';
import { ImageBackground, StyleSheet, TouchableHighlight } from 'react-native';
import { Text, View } from 'react-native';
import { Button, Header, Icon, Input, Overlay } from 'react-native-elements';
import { appName, back, emailText, insertEmail, insertPassword, insertYourName, invalidEmail, 
  invalidName, 
          invalidPassword, name, passwordText, register, registerSucessfull, unknowError, userALreadyRegistered } from '../../constants/Texts';
import { ApiEndpoints, API_SERVER, RequestMethods } from '../../globals/backend-configs';
import { MAX_USER_PASSWORD, MIN_USER_PASSWORD, regexEmail } from '../../globals/constraints';
import { loginView, signup } from '../../navigation/routes';
import * as status  from 'http-status'

export default function Signin({navigation}) {

  let [loading,changeLoadingState] = React.useState(Boolean);
  let [emailLabel,changeEmailLabel] = React.useState(String);
  let [passwordLabel,changePasswordLabel] = React.useState(String);
  let [userEmail,changeUserEmail] = React.useState(String);
  let [userName,changeName] = React.useState(String);
  let [nameLabel,changeNameLabel] = React.useState(String);
  let [isNameValid,changeIsValidName] = React.useState(Boolean);
  let [userPassword,changeUserPassword] = React.useState(String);
  let [isEmailValid,changeUserEmailValid] = React.useState(Boolean);
  let [isPassworValid,changeUserPasswordValid] = React.useState(Boolean);
  let [registerMessage, changeRegisterMessage] = React.useState(String);
  let [visible, setVisible] = React.useState(false);

  const toggleOverlay = ()=> {
    setVisible(!visible);
  
  };
  return (
    <View style={styles.container}>  
    <ImageBackground source={require('../../assets/images/signup_background.png')}
                     resizeMode='cover'
    style={styles.container}>
   <Header
  leftComponent={<TouchableHighlight
    activeOpacity={0.6}
    onPress={() => navigation.goBack()}>
    <Text style = {styles.footerTextPrimary}>{back}</Text>
    </TouchableHighlight>}
  centerComponent={{ text: appName,style: { color: '#fff' } }}
/>  
     <View style={styles.separator1} />
     <Text style = {styles.title}>{register}</Text>
     <Text style = {styles.separator2}/>
     <Text style = {styles.separator2}/>
     <Text style = {styles.title}>{name}</Text>
     <Overlay isVisible={visible} onBackdropPress={toggleOverlay}>
        <Text>{registerMessage}</Text>
      </Overlay>

      <Input  
  style ={{ color:'black'}}
  placeholder= {insertYourName}
  leftIcon={
    <Icon style={styles.inputs}
      name='email'
      size={20}
      color='black'
    />
  }label={nameLabel}
  onChangeText={(name) => {
    if (!validateName(name)){
      changeNameLabel(invalidName);
    }else{
      changeNameLabel('');
      changeIsValidName(true);
      changeName(name);
    }
  }}
  />

<Text style = {styles.title}>{emailText}</Text>


     <Input  
  label = {emailLabel}
  style ={{ color:'black'}}
  placeholder= {insertEmail}
  leftIcon={
    <Icon style={styles.inputs}
      name='email'
      size={20}
      color='black'
    />
  }
  onChangeText={(email) => {
    if (!validateEmail(email)){
      changeEmailLabel(invalidEmail);
    }else{
      changeEmailLabel('');
      changeUserEmailValid(true);
      changeUserEmail(email);
    }
  }}
  />
    <Text style = {styles.title}>{passwordText}</Text>
  <Input style ={{ color:'black'}}
  label = {passwordLabel}
  placeholder={insertPassword}

  leftIcon={
    <Icon style={styles.inputs}
      name='lock'
      size={20}
      color='black'
    />
  } 
  onChangeText={(password) => {
    if(!validatePassword(password)){
      changePasswordLabel(invalidPassword)
    }else{
      changePasswordLabel('')
      changeUserPasswordValid(true);
      changeUserPassword(password);
    }
  }

}
  />

     <View style={styles.separator1}/>
     <View style={styles.button}>
      <Button 
      loading={loading}
      title={register}
              onPress={()=>{
                console.log(isEmailValid);
                console.log(isPassworValid);

                if(isEmailValid && isPassworValid && isNameValid){
                  changeLoadingState(true);
                  fetch(`${API_SERVER}${ApiEndpoints.SIGNUP}`, {
                     method: RequestMethods.POST,
                     headers: { 'Content-Type': 'application/json' },
                      body: JSON.stringify({  email: userEmail,
                                              password:userPassword,
                                              name:userName})
                  })
                  .then((response)=>{
                    console.log(response.status);
                   changeLoadingState(false); 
                   switch(response.status){
                    case status.CREATED:{
                      changeRegisterMessage(registerSucessfull);
                      toggleOverlay();
                      navigation.navigate(loginView);
                      break;

                    }
                    case status.FORBIDDEN:{
                      changeRegisterMessage(userALreadyRegistered);
                      toggleOverlay();
                      break;
                     }
                     
                     default:{
                      changeRegisterMessage(unknowError);
                      toggleOverlay();
                     }
                   }
                  })
                 
                  .catch((error) => {
                     console.error(error);
                  });
               
                }
              }}/>
          <View style={[styles.container]}>
    
  
  </View>
    </View>

    <View><Text style = {styles.footerTextSecondary}>Já possui Conta ? Clique
     
<TouchableHighlight
  activeOpacity={0.6}
  style={styles.footerTextPrimary}
  onPress={() => navigation.navigate(loginView)}>
<Text style = {styles.footerTextPrimary}>Aqui</Text>
</TouchableHighlight> e faça login</Text></View>
    </ImageBackground>
    </View>
  );

}


function validateEmail(email:string):boolean{
  return  RegExp(regexEmail).test(email);
}

function validatePassword(password:string):boolean{
  return !(password.length < MIN_USER_PASSWORD || password.length> MAX_USER_PASSWORD);
}

function validateName(name:string):boolean{
  return !(name.length < MIN_USER_PASSWORD || name.length> MAX_USER_PASSWORD);
}




const styles = StyleSheet.create({
  title: {   
    paddingLeft: 20,   
    fontSize: 23,
    fontWeight: 'bold',

  },
  subTitle:{
  fontSize:18,
  fontWeight:'normal',
  },
  separator1: {
    marginVertical: 25,
    width: '100%',
  },
  separator2: {
    marginVertical: 10,
    height: 1,
    width: '100%',
  },
  container: {
    flex: 1,
    width:'100%'
  },

button:{
    padding:10,
    flex:1,
    height:'100%',
    width:'60%',
    alignSelf:'center'
},

inputs:{
    paddingLeft:10,
    alignItems:'center',
    flexDirection:'row'
},
footerTextPrimary:{
color:'black',
fontSize:18,
alignSelf:'flex-end',
},

footerTextSecondary:{
    color:'black',
    fontSize:18
 }

});


