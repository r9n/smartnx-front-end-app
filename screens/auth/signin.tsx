import * as React from 'react';
import { ImageBackground, StyleSheet, TouchableHighlight } from 'react-native';
import { Text, View } from 'react-native';
import { Button, Header, Icon, Input, Overlay } from 'react-native-elements';
import { appName, back, emailText, insertEmail, insertPassword, invalidEmail, 
  invalidEmailOrPassword, 
          invalidPassword, login, loginSuccessfull, passwordText, unknowError, userNotFound } from '../../constants/Texts';
import { User } from '../../entities/user.entity';
import { ApiEndpoints, API_SERVER, RequestMethods } from '../../globals/backend-configs';
import { MAX_USER_PASSWORD, MIN_USER_PASSWORD, regexEmail } from '../../globals/constraints';
import { loggedUser } from '../../globals/objects';
import { home, loginView, signup } from '../../navigation/routes';
import * as status  from 'http-status'

export default function Signin({navigation}) {

  let [loading,changeLoadingState] = React.useState(Boolean);
  let [emailLabel,changeEmailLabel] = React.useState(String);
  let [passwordLabel,changePasswordLabel] = React.useState(String);
  let [userEmail,changeUserEmail] = React.useState(String);
  let [userPassword,changeUserPassword] = React.useState(String);
  let [isEmailValid,changeUserEmailValid] = React.useState(Boolean);
  let [isPassworValid,changeUserPasswordValid] = React.useState(Boolean);
  let [loginMessage, changeLoginMessage] = React.useState(String);
  let [visible, setVisible] = React.useState(false);
  const toggleOverlay = ()=> {
    setVisible(!visible);
  
  };
  return (
    <View style={styles.container}>  
    <ImageBackground source={require('../../assets/images/signup_background.png')}
                     resizeMode='cover'
    style={styles.container}>

   <Header
  leftComponent={<TouchableHighlight
    activeOpacity={0.6}
    onPress={() => navigation.goBack()}>
    <Text style = {styles.footerTextPrimary}>{back}</Text>
    </TouchableHighlight>}
  centerComponent={{ text: appName,style: { color: '#fff' } }}
/>  
     <View style={styles.separator1} />
     <Text style = {styles.title}>{login}</Text>
     <Text style = {styles.separator2}/>
     <Text style = {styles.separator2}/>
     <Text style = {styles.title}>{emailText}</Text>
     <Overlay isVisible={visible} onBackdropPress={toggleOverlay}>
        <Text>{loginMessage}</Text>
      </Overlay>
    
     <Input  
  label = {emailLabel}
  style ={{ color:'black'}}
  placeholder= {insertEmail}
  leftIcon={
    <Icon style={styles.inputs}
      name='email'
      size={20}
      color='black'
    />
  }
  onChangeText={(email) => {
    if (!validateEmail(email)){
      changeEmailLabel(invalidEmail);
    }else{
      changeEmailLabel('');
      changeUserEmailValid(true);
      changeUserEmail(email);
    }
  }}
  />
    <Text style = {styles.title}>{passwordText}</Text>
  <Input style ={{ color:'black'}}
  label = {passwordLabel}
  placeholder={insertPassword}

  leftIcon={
    <Icon style={styles.inputs}
      name='lock'
      size={20}
      color='black'
    />
  } 
  onChangeText={(password) => {
    if(!validatePassword(password)){
      changePasswordLabel(invalidPassword)
    }else{
      changePasswordLabel('')
      changeUserPasswordValid(true);
      changeUserPassword(password);
    }
  }

}
  />

     <View style={styles.separator1}/>
     <View style={styles.button}>
      <Button 
      loading={loading}
      title={login}
              onPress={()=>{
                console.log(isEmailValid);
                console.log(isPassworValid);

                if(isEmailValid && isPassworValid){
                  changeLoadingState(true);
                  fetch(`${API_SERVER}${ApiEndpoints.SIGNIN}`, {
                     method: RequestMethods.POST,
                     headers: { 'Content-Type': 'application/json' },
                      body: JSON.stringify({ email: userEmail,password:userPassword})
                   
                  })
                  .then( (response)=>{
                   changeLoadingState(false); 
                   switch(response.status){
                    case status.CREATED:{
                      changeLoginMessage(loginSuccessfull);
                      toggleOverlay();
                      response.json().then((json)=>{
                        loggedUser.fromJson(json);
                
                      navigation.navigate(home);
                  
                      })
                      break;
  
                    }
                     case status.FORBIDDEN:{
                      changeLoginMessage(invalidEmailOrPassword)
                      toggleOverlay();
                      break
                     }
                     case status.NOT_FOUND:{
                      changeLoginMessage(userNotFound)
                      toggleOverlay();
                      break;
                     }
                     
                     default:{
                      changeLoginMessage(unknowError)
                      toggleOverlay();
                     }
                   }
                  })
                 
                  .catch((error) => {
                     console.error(error);
                  });
               
                }
              }}/>
          <View style={[styles.container]}>
    
  
  </View>
    </View>

    <View><Text style = {styles.footerTextSecondary}>Não possui Conta ? Clique
     
<TouchableHighlight
  activeOpacity={0.6}
  style={styles.footerTextPrimary}
  onPress={() => navigation.navigate(signup)}>
<Text style = {styles.footerTextPrimary}>Aqui</Text>
</TouchableHighlight> e cadastre-se</Text></View>
    </ImageBackground>
    </View>
  );

}


function validateEmail(email:string):boolean{
  return  RegExp(regexEmail).test(email);
}

function validatePassword(password:string):boolean{
  return !(password.length < MIN_USER_PASSWORD || password.length> MAX_USER_PASSWORD);
}



const styles = StyleSheet.create({
  title: {   
    paddingLeft: 20,   
    fontSize: 23,
    fontWeight: 'bold',

  },
  subTitle:{
  fontSize:18,
  fontWeight:'normal',
  },
  separator1: {
    marginVertical: 25,
    width: '100%',
  },
  separator2: {
    marginVertical: 10,
    height: 1,
    width: '100%',
  },
  container: {
    flex: 1,
    width:'100%'
  },

button:{
    padding:10,
    flex:1,
    height:'100%',
    width:'60%',
    alignSelf:'center'
},

inputs:{
    paddingLeft:10,
    alignItems:'center',
    flexDirection:'row'
},
footerTextPrimary:{
color:'black',
fontSize:18,
alignSelf:'flex-end',
},

footerTextSecondary:{
    color:'black',
    fontSize:18
 }

});


