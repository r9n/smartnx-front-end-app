import * as React from 'react';
import {  ImageBackground, StyleSheet } from 'react-native';
import { Text, View } from 'react-native';
import { Button} from 'react-native-elements';

import { Avatar, Header } from 'react-native-elements';
import { appName, changeAvatar, emailText, info, login, name, 
           welcome } from '../constants/Texts';


import { ApiEndpoints, API_SERVER } from '../globals/backend-configs';
import { loggedUser } from '../globals/objects';
import { ImagePicker } from 'expo';

export default function Home({navigation}) {
  
  return (
    <View style={styles.avatar}>  
    <ImageBackground source={require('../assets/images/signup_background.png')}
                     resizeMode='cover'
    style={styles.container}>

   <Header  
   
  centerComponent={{ text: appName,style: { color: '#fff' } }}
  
  /> 
      <Text style = {styles.separator2}/>
       
<Text style = {styles.title}>{welcome+` ${loggedUser.name}`}</Text>

     <View style={styles.avatar} />
     <Text style = {styles.separator2}/>
     <Text style = {styles.separator2}/>
     <View style={styles.avatar}>

       <Avatar
     size='xlarge'
  rounded
  source={{
    uri:`${API_SERVER}${ApiEndpoints.USER_AVATAR}${loggedUser.id}`,
    method:'GET'
  }}
/> 
<Text style = {styles.separator2}/>

<Button title={changeAvatar}

/>

</View>
<Text style = {styles.separator2}/>
<Text style = {styles.separator2}/>

<Text style = {styles.title2}>{info} :</Text>

<Text style = {styles.separator2}/>

<Text style = {styles.subTitle}>{`${name}:  ${loggedUser.name}`}</Text>

<Text style = {styles.subTitle}>{`${emailText}:  ${loggedUser.email}`}</Text>
<Text style = {styles.subTitle}>{`Ativo no sistema ?:  ${loggedUser.isActive?'Sim':'Não'}`}</Text>
<Text style = {styles.subTitle}>{`Permissão atual:  ${loggedUser.role}`}</Text>
<Text style = {styles.subTitle}>{`DataBase id:  ${loggedUser.id}`}</Text>

<Text style = {styles.subTitle}>{`DataBase id:  ${loggedUser.id}`}</Text>
    </ImageBackground>
    </View>
  );

}


const styles = StyleSheet.create({
  title: {   
    paddingLeft: 20,   
    fontSize: 23,
    fontWeight: 'bold',
    color: '#000000'
  },
  title2:{   
    paddingLeft: 20,   
    fontSize: 23,
    fontWeight: 'bold',
    color: '#cc3399'
  },
  subTitle: {   
    paddingLeft: 20,   
    fontSize: 20,
    fontWeight: 'bold',
    color: '#cc3399'

  },
  separator1: {
    marginVertical: 25,
    width: '100%',
  },
  separator2: {
    marginVertical: 10,
    alignSelf:'auto',
    height: 1,
    width: '100%',
  },
  container: {
    flex: 1,
    width:'100%'
  },
  avatar:{
      alignSelf :'center',
      alignItems:'center'
      
  },
  listItemTitle:{fontSize:40,color:'rgb(0,0,0)'},
  listItemSubTitle:{fontSize:20,color:'rgb(0,0,0)'},
  button:{
    padding:10,
    flex:1,
    height:'100%',
    width:'60%',
    alignSelf:'center'
},

});


