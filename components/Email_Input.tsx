import * as React from "react";
import { Icon, Input, InputProps } from "react-native-elements";

export function InputEmail(props:InputProps) {
  return <Input
  {...props}
  leftIcon={
    <Icon
      name='email'
      size={20}
      color='black'
    />
  }
  />
}